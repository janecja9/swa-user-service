//package fel.cvut.cz.swa.userservice.persistance.repository;
package cz.cvut.fel.swa.userservice.persistance.repository;

//import fel.cvut.cz.swa.userservice.persistance.model.Users;
import cz.cvut.fel.swa.userservice.persistance.model.Users;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<Users, Long> {

    Users findById(long id);
}
