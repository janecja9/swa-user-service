//package fel.cvut.cz.swa.userservice;
package cz.cvut.fel.swa.userservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@EnableJpaRepositories("cz.cvut.fel.swa.userservice.persistance.repository")
@EntityScan("cz.cvut.fel.swa.userservice.persistance.model")
@SpringBootApplication
public class UserserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserserviceApplication.class, args);
	}

}
