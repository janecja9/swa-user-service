//package fel.cvut.cz.swa.userservice.persistance.model;
package cz.cvut.fel.swa.userservice.persistance.model;

import jakarta.persistence.*;

@Entity
public class Users {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(nullable = false)
    private String username;
    @Column(nullable = false)
    private String email;


    public Users() {

    }

    public Users(String username, String email) {
        this.username = username;
        this.email = email;
    }
    public String getUsername() {
        return username;
    }
    public String getEmail() {
        return email;
    }

    public long getId() {
        return id;
    }
}
