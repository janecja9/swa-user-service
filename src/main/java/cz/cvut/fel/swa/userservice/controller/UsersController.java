//package fel.cvut.cz.swa.userservice.controller;
package cz.cvut.fel.swa.userservice.controller;

import cz.cvut.fel.swa.userservice.persistance.model.Users;
//import fel.cvut.cz.swa.userservice.persistance.model.Users;
import cz.cvut.fel.swa.userservice.persistance.repository.UserRepository;
//import fel.cvut.cz.swa.userservice.persistance.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class UsersController {
    private final UserRepository usersRepository;
    @GetMapping("/users/{usersId}")
    @Operation(summary = "Get users by id", description = "Get users by id")
    @ApiResponse(responseCode = "200", description = "User was successfully found")
    @ApiResponse(responseCode = "404", description = "User was not found", content = @Content)
    public ResponseEntity<Users> getUsersById(@PathVariable int usersId) {
        var users = usersRepository.findById(usersId);

        if (users == null) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok(users);
        }

    }

    @GetMapping("/users")
    public Iterable<Users> getAllUsers() {
        return usersRepository.findAll();
    }

}
